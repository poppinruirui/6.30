﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class CCosmosSkeleton : MonoBehaviour {

    public SkeletonGraphic _skeletonGraphic;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetMaterialBySkinId( int nSkinId )
    {
        Material mat = ShoppingMallManager.s_Instance.CreateMaterialForSkeleton( nSkinId );

    }
}
