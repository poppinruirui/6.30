﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBoWen : MonoBehaviour
{

    static Vector3 vecTempScale = new Vector3();
    public SpriteRenderer _srMain;

    //public CBoWenGroup[] m_aryBoWenGroup;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetPos(Vector3 pos)
    {
        this.transform.localPosition = pos;
    }

    public void SetScale(float fScale)
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetColor(Color color)
    {
        _srMain.color = color;
    }

    public float GetSize()
    {
        return _srMain.bounds.size.x;
    }
}