﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTiaoZi_YanMie : CTiaoZi {

    static Vector3 vecTempScale = new Vector3();

    public GameObject _goMainContainer;
    public GameObject _goTxt;

  

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetMainContainerScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        _goTxt.transform.localScale = vecTempScale;
    }

    public float GetMainContainerScale()
    {
        return vecTempScale.x;
    }

   
}
