﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRoleManager : MonoBehaviour {

    public static CRoleManager s_Instance = null;

    public enum eRoleFaceType
    {
        common,          // 常规
        using_skill,     // 使用技能
        being_affected,  // 被影响
    };

    public float m_fEarShakeInterval = 0.1f;
    public float m_fEarShakeAmount = 5f;

    public float m_fHairShakeAmount = 5f;

    public Sprite[] m_aryFaceSprites;

    public Vector2 m_vecEar0_CommonPos = new Vector2();
    public float m_fEar0_CommonRotation = 0;
    public Vector2 m_vecEar1_CommonPos = new Vector2();
    public float m_fEar1_CommonRotation = 0;

    public Vector2 m_vecEar0_AffectedPos = new Vector2();
    public float m_fEar0_AffectedRotation = 0;
    public Vector2 m_vecEar1_AffectedPos = new Vector2();
    public float m_fEar1_AffectedRotation = 0;

    public Vector2 m_vecEar0_SkillPos = new Vector2();
    public float m_fEar0_SkillRotation = 0;
    public Vector2 m_vecEar1_SkillPos = new Vector2();
    public float m_fEar1_SkillRotation = 0;
    internal readonly Vector3 m_vecEar0_AffectPos;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Sprite GetFaceSpriteByType( eRoleFaceType type )
    {
        return m_aryFaceSprites[(int)type];
    }





}
