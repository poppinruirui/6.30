﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes2D;

public class CArc : MonoBehaviour {

    public Shape[] m_aryArcs;
    public float m_fArcLength;

    public GameObject _goArcsContianer;
    static Vector3 vecTempScale = new Vector3();

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Begin( float fScale )
    {
        this.gameObject.SetActive(true);

        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
        for ( int i = 0; i < m_aryArcs.Length; i++ )
        {
            Shape shape = m_aryArcs[i];
            shape.gameObject.SetActive(false);
        }
    }

    int s_Shit = 0;
    public void ShowArc( int nIndex )
    {
        Shape shape = m_aryArcs[nIndex];
        shape.gameObject.SetActive( true );
    }

    public void End()
    {
        s_Shit = 0; ;
        this.gameObject.SetActive( false );
    }
}
