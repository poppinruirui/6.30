﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRoleBall : MonoBehaviour {

    CRoleManager.eRoleFaceType m_eCurStatus = CRoleManager.eRoleFaceType.common;

    public GameObject _containerEar0;
    public GameObject _containerEar1;
    public GameObject _goEar0;    
    public GameObject _goEar1;


    static Vector3 vecTempPos = new Vector3();

    public SpriteRenderer _srFace;

    public SpriteRenderer _srHair;
    public SpriteRenderer _srHair1;
    public SpriteRenderer _srHair2;  


    public SpriteRenderer _srEye0;
    public SpriteRenderer _srEye1;
    public SpriteRenderer _srSunGlasses;
    public SpriteRenderer _srBg;

    public CFrameAnimationEffect _effect;


    int m_nHairLoopStatus = 0;
    float m_fHairAngle = 0;

    int m_nHair2LoopStatus = 0;
    float m_fHair2Angle = 0;

    int m_nSunGlassesStatus = 0;
    float m_fSunGlassesMoveAmount = 0;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        EyeLoop();
        HairLoop();
        SunGlassesLoop();
	}

    private void FixedUpdate()
    {
        EarLoop();
    }

    public void SetColor( Color color )
    {
        _srBg.color = color;
        _effect.SetColor( color );
    }

    void EyeLoop()
    {
        float fRadius = 0.08f;
        vecTempPos.x = PlayerAction.s_Instance._joystick_movement.x * fRadius;
        vecTempPos.y = PlayerAction.s_Instance._joystick_movement.y * fRadius;
        _srEye0.transform.localPosition = vecTempPos;
        _srEye1.transform.localPosition = vecTempPos;
    
    }

    void SunGlassesLoop()
    {
        float fMaxMoveAmount = 0.2f;
        float fMoveSpeed = 0.1f;
        if ( m_nSunGlassesStatus == 0 )
        {
            vecTempPos = _srSunGlasses.transform.localPosition;
            float fMoveAmount = Time.deltaTime * fMoveSpeed;
            vecTempPos.y += fMoveAmount;
            m_fSunGlassesMoveAmount += fMoveAmount;
            if ( m_fSunGlassesMoveAmount > fMaxMoveAmount )
            {
                m_nSunGlassesStatus = 1;
            } 
            _srSunGlasses.transform.localPosition = vecTempPos;
        }
        else
        {
            vecTempPos = _srSunGlasses.transform.localPosition;
            float fMoveAmount = Time.deltaTime * fMoveSpeed;
            vecTempPos.y -= fMoveAmount;
            m_fSunGlassesMoveAmount -= fMoveAmount;
            if (m_fSunGlassesMoveAmount < -fMaxMoveAmount)
            {
                m_nSunGlassesStatus = 0;
            }
            _srSunGlasses.transform.localPosition = vecTempPos;
        }


    }

    void HairLoop()
    {
        if ( m_nHairLoopStatus == 0 )
        {
            m_fHairAngle -= Time.deltaTime * 5f;
            if ( m_fHairAngle < -5 )
            {
                m_nHairLoopStatus = 1;
            }
        }
        else
        {
            m_fHairAngle += Time.deltaTime * 5f;
            if (m_fHairAngle > 0)
            {
                m_nHairLoopStatus = 0;
            }
        }
        _srHair.transform.localRotation = Quaternion.identity;
        _srHair.transform.Rotate(0.0f, 0.0f, m_fHairAngle);    



        if (m_nHair2LoopStatus == 0)
        {
            m_fHair2Angle += Time.deltaTime * 2f;
            if (m_fHair2Angle >= 5)
            {
                m_nHair2LoopStatus = 1;
            }
        }
        else
        {
            m_fHair2Angle -= Time.deltaTime * 2f;
            if (m_fHair2Angle <= 0 )
            {
                m_nHair2LoopStatus = 0;
            }
        }
        _srHair1.transform.localRotation = Quaternion.identity;
        _srHair1.transform.Rotate(0.0f, 0.0f, m_fHair2Angle);   

        _srHair2.transform.localRotation = Quaternion.identity;
        _srHair2.transform.Rotate(0.0f, 0.0f, m_fHair2Angle);   

    }



    //   _srNose.transform.localRotation = Quaternion.identity;
    //   _srNose.transform.Rotate(0.0f, 0.0f, m_fNoseAngle);     


    public void ChangeFace(CRoleManager.eRoleFaceType type)
    {
        m_eCurStatus = type;
        m_nEarActionStatus = 1;
        m_fEarLoopTimeElapse = 0;

        _srFace.sprite = CRoleManager.s_Instance.GetFaceSpriteByType(type);

        switch (type)
        {
            case CRoleManager.eRoleFaceType.common:
                {
                    _containerEar0.transform.localPosition = CRoleManager.s_Instance.m_vecEar0_CommonPos;
                    _containerEar0.transform.localRotation = Quaternion.identity;
                    _containerEar0.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar0_CommonRotation);

                    _containerEar1.transform.localPosition = CRoleManager.s_Instance.m_vecEar1_CommonPos;
                    _containerEar1.transform.localRotation = Quaternion.identity;
                    _containerEar1.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar1_CommonRotation);
                }
                break;

            case CRoleManager.eRoleFaceType.being_affected:
                {
                    _containerEar0.transform.localPosition = CRoleManager.s_Instance.m_vecEar0_AffectedPos;
                    _containerEar0.transform.localRotation = Quaternion.identity;
                    _containerEar0.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar0_AffectedRotation);

                    _containerEar1.transform.localPosition = CRoleManager.s_Instance.m_vecEar1_AffectedPos;
                    _containerEar1.transform.localRotation = Quaternion.identity;
                    _containerEar1.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar1_AffectedRotation);
                }
                break;

            case CRoleManager.eRoleFaceType.using_skill:
                {
                    _containerEar0.transform.localPosition = CRoleManager.s_Instance.m_vecEar0_SkillPos;
                    _containerEar0.transform.localRotation = Quaternion.identity;
                    _containerEar0.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar0_SkillRotation);

                    _containerEar1.transform.localPosition = CRoleManager.s_Instance.m_vecEar1_SkillPos;
                    _containerEar1.transform.localRotation = Quaternion.identity;
                    _containerEar1.transform.Rotate(0.0f, 0.0f, CRoleManager.s_Instance.m_fEar1_SkillRotation);
                }
                break;

        } // end switch
    } //end ChangeFace
       
        float m_fEarLoopTimeElapse = 0;         float m_fEarActionTimeElapse = 0;         int m_nEarActionStatus = 0;          void EarLoop()         {             if ( m_nEarActionStatus == 0 )             {                 m_fEarLoopTimeElapse += Time.fixedDeltaTime;                 if ( m_fEarLoopTimeElapse >= 5 )                 {                     m_fEarLoopTimeElapse = 0;                     m_nEarActionStatus = 1;                 }             }

        if (m_nEarActionStatus > 0)
        {
            if (m_nEarActionStatus % 2 != 0)
            {
                _goEar0.transform.localRotation = Quaternion.identity;
                _goEar0.transform.Rotate(0.0f, 0.0f,CRoleManager.s_Instance.m_fEarShakeAmount);

                _goEar1.transform.localRotation = Quaternion.identity;
                _goEar1.transform.Rotate(0.0f, 0.0f, -CRoleManager.s_Instance.m_fEarShakeAmount);
            }
            else
            {
                _goEar0.transform.localRotation = Quaternion.identity;
                _goEar0.transform.Rotate(0.0f, 0.0f, 0);

                _goEar1.transform.localRotation = Quaternion.identity;
                _goEar1.transform.Rotate(0.0f, 0.0f, 0);
            }

            m_fEarActionTimeElapse += Time.fixedDeltaTime;
            if (m_fEarActionTimeElapse >= CRoleManager.s_Instance.m_fEarShakeInterval)
            {
                m_nEarActionStatus++;
                m_fEarActionTimeElapse = 0;
            }
        }// end  if (m_nEarActionStatus > 0)              if ( m_nEarActionStatus > 4 )             {                 m_nEarActionStatus = 0;             }         }          


} // end class
