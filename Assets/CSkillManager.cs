﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSkillManager : MonoBehaviour {

    public Player m_Player = null;

    Thorn[] m_lstBabaThorn = new Thorn[8];

    static Vector3 vec3Temp = new Vector3();
    static Vector2 vec2Temp = new Vector2();

    public enum eSkillType
    {
        lababa, // 拉粑粑
    };

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UseSkill(eSkillType type )
    {
        switch(type)
        {
            case eSkillType.lababa:
                {
                    UseSkill_LaBaba();
                }
                break;
        }
    }

    void UseSkill_LaBaba(  )
    {
        /*
        // 计算出当前队伍的行进方向，然后分别找出x坐标和y坐标上落在（运行方向）最后面的球球，取平均位置，即是粑粑的出生位置
        // 计算出整个队伍的球球的平均中心点
        vec2Temp = PlayerAction.s_Instance.GetWorldCursorPos() - m_Player.GetBallsCenter();
        vec2Temp.Normalize();
        List<Ball> lstBalls = m_Player.GetBallList();
        bool bFirst = true;
        float fTailX = 0f;
        float fTailY = 0f;
        int nX = 0;
        int nY = 0;
        for ( int i = 0; i < lstBalls.Count; i++ )
        {
            Ball ball = lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }

            float fX = ball.GetPos().x;
            float fY = ball.GetPos().y;

            if (bFirst)
            {
                fTailX = fX;
                fTailY = fY;
                nX = i;
                nY = i;
                bFirst = false;
            }

            if (vec2Temp.x > 0) // 队伍沿x轴正方向行进
            {
                if (fTailX > fX)
                {
                    fTailX = fX;
                    nX = i;
                }
            }
            else// 队伍沿x轴负方向行进
            {
                if (fTailX < fX)
                {
                     fTailX = fX;
                    nX = i;
                }
            }

            if (vec2Temp.y > 0)// 队伍沿y轴正方向行进
            {
                if (fTailY > fY)
                {
                    fTailY = fY;
                    nY = i;
                }
            }
            else// 队伍沿y轴负方向行进
            {
                if (fTailY < fY)
                {
                    fTailY = fY;
                    nY = i;
                }
            }
        }

        Ball ball1 = lstBalls[nX];
        Ball ball2 = lstBalls[nY];

        int nIndex  = GetAvailableBabaThornIndex();
       
        // 粑粑的喷射方向与队伍的行进方向相反
        vec2Temp.x = -vec2Temp.x;
        vec2Temp.y = -vec2Temp.y;

        // baba start pos
        vec3Temp.x = (ball1.GetPos().x + ball2.GetPos().x) / 2f;
        vec3Temp.y = (ball1.GetPos().y + ball2.GetPos().y) / 2f;
        vec3Temp.z = 0f;
  
        byte[] bytes = StringManager.ReuseBlob( 24 );
        StringManager.BeginPushData(bytes);
        StringManager.PushData_Byte((byte)CSkillManager.eSkillType.lababa );
        StringManager.PushData_Byte((byte)nIndex);
        StringManager.PushData_Float(vec2Temp.x);
        StringManager.PushData_Float(vec2Temp.y);
        StringManager.PushData_Float(vec3Temp.x);
        StringManager.PushData_Float(vec3Temp.y);
        m_Player.CastSkill(bytes);
        */
    }

   

    public void CastSkill_Lababa(byte[] bytes)
    {
        /*
        StringManager.BeginPopData(bytes);
        StringManager.SetCurPointerPos(sizeof(byte));
        int nThornIndex = StringManager.PopData_Byte();
        Thorn thorn = GetBabaThornByIndex(nThornIndex);
        if (thorn == null)
        {
            thorn = NewBabaThorn(nThornIndex);
        }
        thorn.SetDead( false );
        thorn.SetLiveTime(Main.s_Instance.m_fLababaLiveTime);

        vec2Temp.x = StringManager.PopData_Float();
        vec2Temp.y = StringManager.PopData_Float();
        vec3Temp.x = StringManager.PopData_Float();
        vec3Temp.y = StringManager.PopData_Float();
        vec3Temp.z = 0f;

        thorn.SetPos(vec3Temp);
        thorn.SetDir(vec2Temp);

        float fSpeed = Main.s_Instance.m_fLaBabaRunDistance / Main.s_Instance.m_fLaBabaRunTime;
        thorn.BeginEject(Main.s_Instance.m_fLaBabaRunDistance, fSpeed, vec3Temp);

        // 队伍中所有的球球消耗体积
        List<Ball> lstBalls = m_Player.GetBallList();
        for ( int i = 0; i < lstBalls.Count; i++ )
        {
            Ball ball = lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            float fArea = ball.GetArea();
            fArea *= (1f - Main.s_Instance.m_fLababaSizeCostPercent);
            float fSize = CyberTreeMath.AreaToSize(fArea, Main.s_Instance.m_nShit);
            if (fSize < Main.BALL_MIN_SIZE)
            {
                fSize = Main.BALL_MIN_SIZE;
            }
            ball.SetSize(fSize);
        }
        */
    }

    public void RecycleBabaThorn( int nThornIndex )
    {
        Thorn thorn = m_lstBabaThorn[nThornIndex];
        thorn.SetDead( true );
    }

    public int GetAvailableBabaThornIndex()
    {
        Thorn thorn = null;
        int nIndex = 0;
        for (int i = 0; i < m_lstBabaThorn.Length; i++)
        {
            nIndex = i;
            thorn = m_lstBabaThorn[i];
            if (thorn == null)
            {
                return nIndex;
            }

            if (thorn.IsDead())
            {
                return nIndex;
            }
        }
        return nIndex;
    }

    public Thorn ReuseBabaThorn()
    {
        Thorn thorn = null;
        int nIndex = 0;
        for (int i = 0; i < m_lstBabaThorn.Length; i++)
        {
            nIndex = i;
            thorn = m_lstBabaThorn[i];
            if (thorn == null)
            {
                break;
            }

            if (thorn.IsDead())
            {
                thorn.SetDead(false); // 复用
                return thorn;
            }
        }
        if (thorn == null)
        {
            thorn = NewBabaThorn(nIndex);
        }
        return thorn;
    }

    public Thorn NewBabaThorn( int nIndex )
    {
		/*
        Thorn thorn = ResourceManager.ReuseThorn();
        thorn.SetOwnerId(m_Player.GetOwnerId());
        thorn.SetThornType(Thorn.eThornType.baba);
        thorn.SetDead(false);
        thorn.SetIndex(nIndex);
        thorn.SetLiveTime( Main.s_Instance.m_fLababaLiveTime );
        m_lstBabaThorn[nIndex] = thorn;
        return thorn;
		*/
		return null;
    }

    public Thorn GetBabaThornByIndex( int nIndex )
    {
        return m_lstBabaThorn[nIndex];
    }
}
