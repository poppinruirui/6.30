﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBlock : MonoBehaviour {

	static Vector3 vecTempPos = new Vector3 ();
	static Vector3 vecTempScale = new Vector3 ();

	int m_nHierarchy = 0;

	public Collider2D _Collider;

	public CBlock[] m_aryChildBlock = new CBlock[4]; // 每个区块含有四个子区块

	CBlockDivide.eBlockPosType m_ePosType = CBlockDivide.eBlockPosType.none;
	CBlockDivide.eBlockPosType m_eParentPosType  =CBlockDivide.eBlockPosType.none;

	uint m_uKey = 0;
	uint m_uParentKey = 0;
    uint m_uGuid = 0;

	CBlock m_blockParent = null;

    public SpriteRenderer m_sprMain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetGuid( uint uGuid )
    {
        m_uGuid = uGuid;
    }

    public uint GetGuid()
    {
        return m_uGuid;
    }

    int m_nBallNumInMe = 0;
    public void ResetAll()
    {
        m_nBallNumInMe = 0;
    }

    public void SetPos( Vector3 pos )
	{
		this.transform.position = pos;
	}

	public Vector3 GetPos()
	{
		return this.transform.position;
	}

	public void SetScale( Vector3 scale )
	{
		this.transform.localScale = scale;
	}

	public Vector3 GetScale()
	{
		return this.transform.localScale;
	}

	public void SetHierarchy( int nHierarchy )
	{
		m_nHierarchy = nHierarchy;
	}

	public int GetHierarchy( )
	{
		return m_nHierarchy;
	}

    public bool IfBallIn()
    {
        return m_nBallNumInMe > 0;
    }


    public void OnTriggerEnter2D( Collider2D collider )
	{
		if (collider.gameObject.tag != "Mouth") {
			Physics2D.IgnoreCollision ( this._Collider,  collider);
			return;
		}

		Ball ball = collider.transform.parent.gameObject.GetComponent<Ball> ();
		if (ball == null) {
			Debug.LogError ("OnTriggerEnter2D ball == null");
			return;
		}

   
        if (m_nBallNumInMe == 0)
        {
            // 判断，如果这个区块已经点亮为MainPlayer的颜色，则无需生成子区块的Block了
            uint uKey = GetKey();
            CBloodStain stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uKey);
            if (stain != null && stain.GetColorId() == ball.GetPlayerId())
            {

            }
            else
            {
                EnableChildBlocks();

                int nColorId = ball.GetPlayerId();
                LightBloodStain(nColorId);

            }

        }
        m_nBallNumInMe++;

    }

    /*
    客观事实：
    1、一个物体如果离开了父（祖）区块，则一定离开了子（孙）区块
    2、一个物体如果在子（孙）区块，则一定在父（祖）区块。
    */

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag != "Mouth")
        {
            Physics2D.IgnoreCollision(this._Collider, collider);
            return;
        }

        Ball ball = collider.transform.parent.gameObject.GetComponent<Ball>();
        if ( ball == null)
        {
            Debug.LogError(" OnTriggerExit2D ball == null");
            return;
        }

        m_nBallNumInMe--;
        if (m_nBallNumInMe == 0)
        {
            DisableChildBlocks();
        }
    }

    public bool CheckIfSameColorParentExist( int nColorId )
	{
        // 必须实时取bloodstain，因为block随时都在销毁回收，它身上无法缓存stain数据的
        /*
		CBloodStain stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode( GetKey() );
		if (stain != null && stain.GetColorId () == nColorId) {
			return true;
		}

		CBlock parent_block = GetParent ();
		if (parent_block == null) {
			return false;
		}

		return parent_block.CheckIfSameColorParentExist ( nColorId ); */

        // 这个判断不能基于Block，因为最新的机制中，Block回收得很及时。
        return false;
	}

	public bool HandleDifferentColorParentExist( CBlock the_block, int nNewColorId)
	{
		// 必须实时取bloodstain，因为block随时都在销毁回收，它身上无法缓存stain数据的
		CBloodStain stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode( GetKey() );
		if (stain != null && stain.GetColorId () != nNewColorId) {
			if ( IsLowest() ) // 正好是最低层级色块，直接替换成最新的颜色即可
			{
				stain.SetColorId ( nNewColorId );
				stain.TryLightParent(); // 尝试点亮父色块
			}
			else // 高于最低层级，打碎(打碎最少执行一次，多的时候是递归操作)
			{
				BreakStain (stain, the_block, stain.GetColorId(), nNewColorId);
			}

			return true;
		}

		CBlock parent_block = GetParent ();
		if (parent_block == null) {
			return false;
		}

		return parent_block.HandleDifferentColorParentExist ( the_block, nNewColorId );
	}

	public bool IsMyAncestor( CBlock higher_block )
	{
		CBlock my_parent = GetParent ();
		if (my_parent == null) {
			return false;
		}

		if (my_parent == higher_block) {
			return true;
		}

		return my_parent.IsMyAncestor ( higher_block );
	}

	public CBloodStain GenerateBloodStainByBlockInfo( ref CBlock block )
	{
		CBloodStain stain = CBloodStainManager.s_Instance.NewBloodStain_SprayMode (block.GetKey());
		stain.SetPos ( block.GetPos() );
		stain.SetScale ( block.GetScale() );
		stain.SetParentKey ( block.GetParentKey() );
		stain.SetPosType ( block.GetPosType() );
		stain.SetParentPosType ( block.GetParentPosType() );
		stain.SetHierarchy ( block.GetHierarchy() );

		return stain;
	}

	// 打碎色块(不是打碎"区块"哈，区块的调度走自己的既有流程)
	public void BreakStain( CBloodStain stain, CBlock the_block, int nCurColorId ,int nNewColorId )
	{
		// 该色块销毁，紧接着生成4个子色块
		if (stain != null) {
            // 	CBloodStainManager.s_Instance.DeleteBloodStain_SprayMode (stain);
            CBloodStainManager.s_Instance.DoFade(stain);
        }

		bool bLastTime = false; // 是否最后一次打碎
		if (the_block.GetHierarchy () - GetHierarchy () == 1) { // 层级刚好相差一级，就是最后一次打碎
			bLastTime = true;
		}

		if (bLastTime) { // 最后一次打碎
			for (int i = 0; i < m_aryChildBlock.Length; i++) {
				CBlock child_block = m_aryChildBlock [i];
				CBloodStain child_stain = GenerateBloodStainByBlockInfo( ref child_block );
				if (child_block == the_block) {
					child_stain.SetColorId (nNewColorId);
				} else {
					child_stain.SetColorId (nCurColorId);
				}
			}

		} else {
			for (int i = 0; i < m_aryChildBlock.Length; i++) {
				CBlock child_block = m_aryChildBlock [i];
				if (the_block.IsMyAncestor (child_block)) { // 要变色的色块在我下面，我暂时不生成子色块，等到最后一次打碎时再生成
					child_block.BreakStain( null, the_block, nCurColorId, nNewColorId ); // 继续打碎（递归操作）
				} else { // 生成子色块
					CBloodStain child_stain = GenerateBloodStainByBlockInfo( ref child_block );
					child_stain.SetColorId ( nCurColorId );
				}
			}
		}

	}
		
	public void LightBloodStain ( int nColorId )
	{
		if ( !IsLowest() ) // 只有最低层级的色块可以直接被游戏世界中的逻辑事件点亮。高于最低层级的各个层级的色块，都是被递归算法点亮的。
		{
			return;
		}

        uint uKey = GetKey();
        CBloodStain stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uKey); 
        if ( stain != null && stain.GetColorId() == nColorId )
        {
            return;
        }
        
        /* ？？根本木有需要作这项判断了，因为一个色块只要点亮，对应的区块（及其碰撞器）就销毁了，根本不会再走到这个函数里面来
        // 该区块以及父区块（包括更高层级的祖区块）已点亮且同色
        bool bSameColorParentExist = CheckIfSameColorParentExist ( nColorId );
		if (bSameColorParentExist) {
			return;
		}
        */

        /* to do 不同色（OtherPlayer所点亮的色块）的处理稍后再弄
		// 该区块的父区块（包括更高层级的祖对象）已点亮且不同色
		bool bDifferentColorParentExist = HandleDifferentColorParentExist( this, nColorId );
		if (bDifferentColorParentExist) {
			return;
		}
        */
        
		// 该区块已存在同色或不同色的色块的情况已处理完毕。可以直接点亮新色块
		stain = CBloodStainManager.s_Instance.NewBloodStain_SprayMode (uKey);
		stain.SetPos ( GetPos() );
		stain.SetScale ( GetScale() );
		stain.SetParentKey ( GetParentKey() );
		stain.SetPosType ( GetPosType() );
		stain.SetParentPosType ( GetParentPosType() );
		stain.SetHierarchy ( GetHierarchy() );
		stain.SetColorId ( nColorId );

        // 这个色块已点亮，其对应的区块可以销毁了(区块及其碰撞器再继续运作，毫无意义，只会白白消耗巨大的性能)
         CBlockDivide.s_Instance.DeleteBlock(this);

        // 判断自己可否与周围的血迹块一起点亮父对象（此操作为递归操作）
        stain.TryLightParent();



    }



	public bool IsLowest()
	{
		if (GetHierarchy () == CBlockDivide.s_Instance.GetMaxHierarchyNo()) {
			return true;
		}
		
		return false;
	}

	public void SetActive( bool bActive )
	{
		this.gameObject.SetActive (bActive);
	}

	public void SetPosType( CBlockDivide.eBlockPosType eType )
	{
		m_ePosType = eType;
	}

	public CBlockDivide.eBlockPosType GetPosType()
	{
		return m_ePosType;
	}

	public void SetParentPosType( CBlockDivide.eBlockPosType eType )
	{
		m_eParentPosType = eType;
	}

	public CBlockDivide.eBlockPosType GetParentPosType()
	{
		return m_eParentPosType;
	}

	public void SetKey( uint uKey )
	{
		m_uKey = uKey;
	}

	public void SetParentKey( uint uParentKey )
	{
		m_uParentKey = uParentKey;
	}

	public void SetParent( CBlock block_parent )
	{
		m_blockParent = block_parent;
	}

	public CBlock GetParent()
	{
		return m_blockParent;
	}

	public uint GetParentKey()
	{
		return m_uParentKey;
	}

	public uint GetKey()
	{
		return m_uKey;
	}
		
	public void EnableChildBlocks() 
	{
		if (IsLowest ()) {
			return;
		}


		CBlock child_block = null;
		int nHierachyNo = GetHierarchy () + 1;
		float fScale = CBlockDivide.s_Instance.GetBlockScaleByHierarchyNo ( nHierachyNo );
		float fHalfScale = fScale / 2f;
		float fZ = -(float)nHierachyNo / 10f;
        CBloodStain stain = null;
		int nIndex = 0;
		child_block = m_aryChildBlock [nIndex];
        uint uKey = GetKey() + (uint)CBlockDivide.eBlockPosType.left_up * CBlockDivide.s_Instance.GetHierarchyKeybase(nHierachyNo);
        stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uKey);
        if (child_block == null && stain == null)
        {
            child_block = CBlockDivide.s_Instance.NewBlock(uKey);
            child_block.SetParent(this);
            child_block.SetPosType(CBlockDivide.eBlockPosType.left_up);
            child_block.SetParentPosType(GetPosType());
            child_block.SetKey(uKey);
            child_block.SetParentKey(GetKey());

            child_block.SetHierarchy(nHierachyNo);
            m_aryChildBlock[nIndex] = child_block;

            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            child_block.SetScale(vecTempScale);
            vecTempPos.x = GetPos().x - fHalfScale;
            vecTempPos.y = GetPos().y + fHalfScale;
            vecTempPos.z = fZ;
            child_block.SetPos(vecTempPos);
        }
        nIndex++; 

		child_block = m_aryChildBlock [nIndex];
        uKey = GetKey() + (uint)CBlockDivide.eBlockPosType.right_up * CBlockDivide.s_Instance.GetHierarchyKeybase(nHierachyNo);
        stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uKey);
        if (child_block == null && stain == null)
        {
            child_block = CBlockDivide.s_Instance.NewBlock(uKey);
            child_block.SetPosType(CBlockDivide.eBlockPosType.right_up);
            child_block.SetParentPosType(GetPosType());
            child_block.SetParent(this);
            child_block.SetKey(uKey);
            child_block.SetParentKey(GetKey());

            child_block.SetHierarchy(nHierachyNo);
            m_aryChildBlock[nIndex] = child_block;
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            child_block.SetScale(vecTempScale);
            vecTempPos.x = GetPos().x + fHalfScale;
            vecTempPos.y = GetPos().y + fHalfScale;
            vecTempPos.z = fZ;
            child_block.SetPos(vecTempPos);
        }
        nIndex++;

		child_block = m_aryChildBlock [nIndex];
        uKey = GetKey() + (uint)CBlockDivide.eBlockPosType.left_down * CBlockDivide.s_Instance.GetHierarchyKeybase(nHierachyNo);
        stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uKey);
        if (child_block == null && stain == null  /*|| stain 不是MainPlayer的颜色  */ ) // 已经点亮本MainPlayer的色块了，就不必再激活碰撞区块
        {
            child_block = CBlockDivide.s_Instance.NewBlock(uKey);
            child_block.SetPosType(CBlockDivide.eBlockPosType.left_down);
            child_block.SetParentPosType(GetPosType());
            child_block.SetParent(this);
            child_block.SetKey(uKey);
            child_block.SetParentKey(GetKey());


            child_block.SetHierarchy(nHierachyNo);
            m_aryChildBlock[nIndex] = child_block;
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            child_block.SetScale(vecTempScale);
            vecTempPos.x = GetPos().x - fHalfScale;
            vecTempPos.y = GetPos().y - fHalfScale;
            vecTempPos.z = fZ;
            child_block.SetPos(vecTempPos);
        }
        nIndex++;

		child_block = m_aryChildBlock [nIndex];
        uKey = GetKey() + (uint)CBlockDivide.eBlockPosType.right_down * CBlockDivide.s_Instance.GetHierarchyKeybase(nHierachyNo);
        stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uKey);
        if (child_block == null && stain == null)
        {
            child_block = CBlockDivide.s_Instance.NewBlock(uKey);
            child_block.SetPosType(CBlockDivide.eBlockPosType.right_down);
            child_block.SetParentPosType(GetPosType());
            child_block.SetKey(uKey);
            child_block.SetParentKey(GetKey());
            child_block.SetParent(this);


            child_block.SetHierarchy(nHierachyNo);
            m_aryChildBlock[nIndex] = child_block;
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1f;
            child_block.SetScale(vecTempScale);
            vecTempPos.x = GetPos().x + fHalfScale;
            vecTempPos.y = GetPos().y - fHalfScale;
            vecTempPos.z = fZ;
            child_block.SetPos(vecTempPos);
        }
        nIndex++;
      
	}

	public void DisableChildBlocks() 
	{
		if (IsLowest ()) {
			return;
		}

		for (int i = 0; i < 4; i++) {
			CBlock child_block = m_aryChildBlock [i];
			if (child_block == null) {
				continue;
			}
			CBlockDivide.s_Instance.DeleteBlock (child_block, true);
			m_aryChildBlock [i] = null;
		}
	}

    public void RipOutChild( CBlockDivide.eBlockPosType ePosType )
    {
        int nIndex = (int)ePosType - 1;
        m_aryChildBlock[nIndex] = null;
    }

    public void SetColliderEnable( bool bEnable)
    {
        _Collider.enabled = bEnable;

        if ( !bEnable )
        {
            m_sprMain.color = Color.gray;
        }
    }
}
